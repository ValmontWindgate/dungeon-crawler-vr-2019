﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBullet : MonoBehaviour {

    private float bulletLifeTime;
    private Player player;
  
	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("GameController").GetComponent<Player>();
        bulletLifeTime = player.GetRange();
        
	}
	
	// Update is called once per frame
	void Update ()
    {        
        DeactiveBullet();		
	}

    private void DeactiveBullet()
    {
        Destroy(this.gameObject, bulletLifeTime);     
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Walls"))
        {
            Destroy(this.gameObject);
        }

        if (collision.CompareTag("enemy"))
        {
            Destroy(this.gameObject);
        }
    }
}
