﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : BaseChar {

    public int strength, dexterity, constitution, intelligence, wisdom, charisma;

    public float mana;
    public float manaRecover;

    protected float tdt;
    private float getInput;
    private Rigidbody rbBullet;
    public float range;

    // Use this for initialization
    public virtual void Start ()
    {
        HP = 3 + constitution;
        ActualHP = HP;
        DMG = 1 + strength;
        speed = 5.0f + ((float)dexterity/2);
        lastShot = 0.0f;
        reload = 1.0f - ((float)dexterity/20);
        bulletSpeed = 150 + dexterity;        
        lastShot = 0.0f;
        tdt = Time.deltaTime;
        manaRecover = 0/tdt;
        mana = intelligence + wisdom;
        range = 1 + Mathf.RoundToInt(dexterity/4);

        if ( character != null)
            anim = character.GetComponent<Animator>();
        left = false;
        right = false;
        up = false;
        down = false;
    }
	
	// Update is called once per frame
	public virtual void Update ()
    {
        if (character != null){
        Movement();
        SetAnimations();
        Shoot();
        }
        mana += manaRecover;
	}


    //Terminar Shoot()
    public override void Shoot()
    {
        float limit = lastShot + reload;        
        if (Time.time > limit)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                bulletSpawnTime = Time.time;                               
                rbBullet = InstanciateObject(bullet, character.transform.position + Vector3.up, 90, 0, -90).GetComponent<Rigidbody>();
                rbBullet.velocity = transform.forward * bulletSpeed * tdt;
                lastShot = Time.time;                
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                bulletSpawnTime = Time.time;               
                rbBullet = InstanciateObject(bullet, character.transform.position + Vector3.up, 90, 180, -90).GetComponent<Rigidbody>();
                rbBullet.velocity = -transform.forward * bulletSpeed * tdt;
                lastShot = Time.time;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                bulletSpawnTime = Time.time;                
                rbBullet = InstanciateObject(bullet, character.transform.position + Vector3.up, 0, 0, 180).GetComponent<Rigidbody>();
                rbBullet.velocity = transform.right * bulletSpeed * tdt;
                lastShot = Time.time;
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                bulletSpawnTime = Time.time;               
                rbBullet = InstanciateObject(bullet, character.transform.position + Vector3.up, 0, 0, 0).GetComponent<Rigidbody>(); ;
                rbBullet.velocity = -transform.right * bulletSpeed * tdt;
                lastShot = Time.time;
            }        
        }
    }
    public override void Movement()
    {
        if(Input.GetAxis("Vertical") > 0.1)
        {
            SetBoolsAnim(false, false, true, false);
            character.transform.Translate(Vector3.forward * speed * tdt);
        } else if (Input.GetAxis("Vertical") < -0.1)
        {
            SetBoolsAnim(false, false, false, true);
            character.transform.Translate(Vector3.back * speed * tdt);
        } else if (Input.GetAxis("Horizontal") > 0.1)
        {
            SetBoolsAnim(false, true, false, false);
            character.transform.Translate(Vector3.right * speed * tdt);
        } else if (Input.GetAxis("Horizontal") < -0.1)
        {
            SetBoolsAnim(true, false, false, false);
            character.transform.Translate(Vector3.left * speed * tdt);
        }
        else
        {
            SetBoolsAnim(false, false, false, false);
        }
    }            

    public override void SetActualHP(int _acHP)
    {
        ActualHP = _acHP;

        if (ActualHP < 1)
        {
            Death(character);
        }
    }

    public override void Death(GameObject _GO)
    {
        Destroy(_GO);
    }
    public virtual int GetActualHP()
    {
        return ActualHP;
    }
    public override void SetBoolsAnim(bool _left, bool _right, bool _up, bool _down)
    {
        if (anim != null)
        base.SetBoolsAnim(_left, _right, _up, _down);
    }
    public override void SetAnimations()
    {
        if (anim != null)
        {
        anim.SetBool("Left", left);
        anim.SetBool("Right", right);
        anim.SetBool("Up", up);
        anim.SetBool("Down", down);   
        }           
    }
    public override GameObject InstanciateObject(GameObject _gameObject, Vector3 _position, float _Xrot, float _Yrot, float _Zrot)
    {
        if (_gameObject != null)
        {
        _gameObject.transform.rotation = Quaternion.Euler(new Vector3(_Xrot, _Yrot, _Zrot));
        GameObject intanceGO = Instantiate(_gameObject, _position, _gameObject.transform.rotation);
        return intanceGO;
        }

        return null;        
    }

    public virtual void SetRange(float _value)
    {
        range = _value;
    }

    public virtual float GetRange()
    {
        return range;
    }
}