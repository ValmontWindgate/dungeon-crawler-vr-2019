﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionAxis : MonoBehaviour {

	public float giro = 90f;
	public Vector3 ejeRotacion = Vector3.up;
	public Space espacio = Space.Self;
	public string ejeInput = "Mouse X";

	void Start ( ) {
	}

	// Update is called once per frame
	void Update () {
		#if UNITY_WEBGL || UNITY_EDITOR || UNITY_STANDALONE_WIN
		transform.Rotate(ejeRotacion * Input.GetAxis(ejeInput) * giro * Time.deltaTime , espacio );
		#endif
	}
}
