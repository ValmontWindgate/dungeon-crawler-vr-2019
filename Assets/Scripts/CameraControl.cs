﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {


    public Camera mainCamera;
    public Transform mainCharacter;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (mainCharacter != null)
        mainCamera.transform.position = new Vector3(mainCharacter.transform.position.x, 1.54f, mainCharacter.transform.position.z - 4);
	}
}
