﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Heart_o_Meter : MonoBehaviour {

    public GameObject heartImage;
    public Transform heartParent;
    private Player player;
    public List<GameObject> _heartz;

    private void Awake() 
    {
        player = GetComponent<Player>();   
    }
	// Use this for initialization
	void Start ()
    {
        HeartControl();
    }

    // Update is called once per frame
    void Update ()
    {
	}


    //CORREGIR-NO FUNCIONA
    public void HeartControl()
    {        
        if (player.ActualHP > 0)
        {
            for(int i = 0; i < player.ActualHP; i++)
            {   _heartz.Capacity++;             
                GameObject temp = Instantiate(heartImage);
               _heartz.Add(temp);              
               _heartz[i].transform.SetParent(heartParent);
               _heartz[i].transform.SetPositionAndRotation(new Vector3((80 + (52 * i)), 1010, 0), heartParent.transform.rotation);
            }
        }  
        
    }

    public void RemoveHearts(int _dmg)
    {       
        int newHP = player.GetActualHP() - _dmg;
        for(int i = player.GetActualHP(); i > newHP; i--)
        {
            Debug.Log ( "Eliminando corazon en índice " + i );
            if (_heartz.Capacity > 0){
            Destroy(_heartz[i -1]);
            _heartz.RemoveRange(i - _dmg, _dmg);
            }
        }
    }

    public void CreateHearts(int hpUP)
    {
        int j = player.ActualHP + hpUP;
        for (int i = player.ActualHP; i < j; i++)
        {
            GameObject temp = Instantiate(heartImage.gameObject);       
            temp.transform.SetParent(heartParent);
            temp.transform.SetPositionAndRotation(new Vector3((40 + (50 * i)), 1010, 0), heartParent.transform.rotation);
            _heartz.Add(temp);
        }
    }
}
