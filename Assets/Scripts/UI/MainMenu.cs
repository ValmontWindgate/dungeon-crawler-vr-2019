﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public GameObject canvasMenu, charCanvas, statCanvas;
	public Animator anim;
	public int creditScene, mainMenu;

	// Use this for initialization
	private void Awake() {


	if (canvasMenu != null && charCanvas != null)
		{
		canvasMenu.SetActive(true);
		charCanvas.SetActive(false);
		statCanvas.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayButton()
	{
		canvasMenu.SetActive(false);
		charCanvas.SetActive(true);
	}

	public void CreditsButton()
	{
		if ( anim != null ) anim.SetBool("CreditScene", true);
		SceneManager.LoadScene(creditScene);
	}

	public void BackToMainMenu()
	{
		anim.SetBool("CreditScene", false);
		SceneManager.LoadScene(mainMenu);
	}

	public void QuitButton()
	{
		Application.Quit();
	}

	public void BackButton()
	{
		canvasMenu.SetActive(true);
		charCanvas.SetActive(false);
	}
}
