﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;

public class Dialogo : MonoBehaviour {

	public GameObject canvas;
	public Text texto;
	public string [ ] frases;
	public int tirada = 0;
	private VRInteractiveItem vrInteractive;
	
	void Start ( ) {
		vrInteractive = gameObject.AddComponent<VRInteractiveItem>( );
		DesactivarCanvas ( );
		vrInteractive.OnClick += AsignarFrase;
		vrInteractive.OnOut += DesactivarCanvas;
	}

	void Update ( ) {
		if ( Input.GetKeyDown ( KeyCode.O ) ) DesactivarCanvas ( );
		if ( Input.GetKeyDown ( KeyCode.P ) ) ActivarCanvas ( );
		if ( Input.GetKeyDown ( KeyCode.C ) ) AsignarFrase ( );
	}

	public void AsignarFrase ( ) {
		if ( tirada != 0 ) return;
		tirada = Random.Range ( 1 , 11 );
		if ( tirada <= 2 ) {
			texto.text = frases [ 0 ];
		}
		else if ( tirada <= 5 ) {
			texto.text = frases [ 1 ];
		}
		else if ( tirada <= 9 ) {
			texto.text = frases [ 2 ];
		}
		else {
			texto.text = frases [ 3 ];
		}

	}

	public void ActivarCanvas ( ) {
		canvas.SetActive ( true );
	}
	public void DesactivarCanvas ( ) {
		canvas.SetActive ( false );
	}

}
