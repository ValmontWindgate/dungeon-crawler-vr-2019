﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

	public Animator animator;
	
	void Start ( ) {
		animator.SetInteger ( "Class" , PlayerControl.main.playerClass );
	}

	void Update ( ) {
		if ( Input.GetMouseButtonDown ( 0 ) || Input.GetButtonDown ( "Fire1") ) {
			animator.SetTrigger ( "Attack" );

			if ( PlayerControl.main.playerClass == 0 ) {
				MeleeAttack ( );
			}
			else if ( PlayerControl.main.playerClass == 1 ) {
				// MeleeAttack ( );
			}
			else if ( PlayerControl.main.playerClass == 2 ) {
				// MeleeAttack ( );
			}

		}
	}

	void MeleeAttack ( ) {
		Collider [ ] targets = Physics.OverlapSphere ( transform.position + transform.forward , 0.5f );
		foreach ( Collider current in targets ) {
			// current.GetComponent<...>( ).Damage ( )
		}
	}

}
