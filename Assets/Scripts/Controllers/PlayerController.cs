﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	// Configuracion de velocidades
	public float velocidad = 6.0f;
	public float giro = 90.0f;
	public float salto = 8.0f; 
	public float gravedad = 20.0f;
	public GameObject camara;

	private Vector3 movimiento = Vector3.zero;
	public Rigidbody rigidbody;

	void Update ( ) {

		// Leo la entrada de datos direccional
		float vertical = Input.GetAxis ( "Vertical" );
		
		// Calculamos movimiento segun la entrada de datos
		movimiento = camara.transform.forward;
		movimiento = new Vector3 ( movimiento.x , 0 , movimiento.z );
		movimiento = movimiento * vertical;

		// Convertimos el movimiento en coordenadas locales, por si hemos girado
		// movimiento = transform.TransformDirection ( movimiento );

		// Multiplicamos por la velocidad del personaje
		movimiento *= velocidad;

		// Salto
		if ( Input.GetButton ( "Jump" ) && 
			Physics.Raycast ( transform.position + Vector3.up , Vector3.down , 1.1f ) ) {
			rigidbody.AddForce ( Vector3.up * 10 , ForceMode.Impulse );
		}

		// Giro
		camara.transform.Rotate ( Input.GetAxis ( "Horizontal" ) * Vector3.up * giro * Time.deltaTime );

		// Movimiento final
		float caida = rigidbody.velocity.y;
		rigidbody.velocity = movimiento;
		rigidbody.velocity = new Vector3 ( rigidbody.velocity.x , caida , rigidbody.velocity.z );

	}

}
