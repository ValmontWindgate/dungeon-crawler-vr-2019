﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetStatsValues : MonoBehaviour {

    private MagePlayer player_Mage;
    private WarriorPlayer player_Warrior;
    private ArcherPlayer player_Archer;
    public GameObject destroyBulletOBJ;
    public Text dmg;
    public Text speed;
    public Text range;
    public Text shotSpeed;
    public Text fireRate;

    // Use this for initialization
    void Start () {
        
        if(player_Mage != null){
           player_Mage = GetComponent<MagePlayer>(); 
        }else if (player_Warrior != null){
            player_Warrior = GetComponent<WarriorPlayer>();
        } else {
            player_Archer = GetComponent<ArcherPlayer>();
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if(player_Mage != null){
        //    SetValues(player_Mage.dmg, player_Mage.speed, player_Mage.GetRange() ,player_Mage.bulletSpeed/10, player_Mage.reload);            
        //}else if (player_Warrior != null){
        //    SetValues(player_Warrior.DMG, player_Warrior.speed, player_Warrior.GetRange() ,player_Warrior.bulletSpeed/10, player_Warrior.reload);
        //} else {
        //   SetValues(player_Archer.DMG, player_Archer.speed, player_Archer.GetRange() ,player_Archer.bulletSpeed/10, player_Archer.reload); 
        //}        
	}

    private void SetValues(float _dmg, float _speed, float _range, float _shotSpeed, float _fireRate)
    {
        dmg.text = _dmg.ToString();
        speed.text = _speed.ToString();
        range.text = _range.ToString();
        shotSpeed.text = _shotSpeed.ToString();
        fireRate.text = _fireRate.ToString();
    }
}
