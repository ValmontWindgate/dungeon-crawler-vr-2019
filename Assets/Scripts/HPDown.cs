﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPDown : MonoBehaviour {

    private Player player;
    private Heart_o_Meter heart_O_Meter;
    
    

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Manager").GetComponent<Player>();
        heart_O_Meter = GameObject.Find("Manager").GetComponent<Heart_o_Meter>();
    }

    // Update is called once per frame
    void Update () {
		
        
	}

    private void HealthDown()
    {
        player.SetActualHP(player.ActualHP - 1);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (player.ActualHP > 0)
            {
                HealthDown();
                heart_O_Meter.RemoveHearts(1);
                Destroy(this.gameObject);
            }
        }
    }
}
