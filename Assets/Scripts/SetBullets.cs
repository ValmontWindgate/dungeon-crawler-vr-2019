﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SetBullets : MonoBehaviour {

    public GameObject prefab;
    public Scene scene02, scene03, scene04, scene05;
    
	// Use this for initialization
	void Start ()
    {
        		

	}
	
	// Update is called once per frame
	void Update ()
    {
        ChangeBullet();
	}

    public void ChangeBullet()
    {
        if(SceneManager.GetActiveScene() == scene02)
        {
            prefab.GetComponent<Animator>().SetBool("Stage02", true);
        }
        if (SceneManager.GetActiveScene() == scene03)
        {
            prefab.GetComponent<Animator>().SetBool("Stage02", false);
            prefab.GetComponent<Animator>().SetBool("Stage03", true);
        }
        if (SceneManager.GetActiveScene() == scene04)
        {
            prefab.GetComponent<Animator>().SetBool("Stage02", false);
            prefab.GetComponent<Animator>().SetBool("Stage03", false);
            prefab.GetComponent<Animator>().SetBool("Stage04", true);
        }
        if (SceneManager.GetActiveScene() == scene04)
        {
            prefab.GetComponent<Animator>().SetBool("Stage02", false);
            prefab.GetComponent<Animator>().SetBool("Stage03", false);
            prefab.GetComponent<Animator>().SetBool("Stage04", false);
            prefab.GetComponent<Animator>().SetBool("Stage05", true);
        }

    }
}
