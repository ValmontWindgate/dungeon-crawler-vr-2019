﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

	public Rigidbody rigidbody;

	void Start ( ) {
		rigidbody.AddRelativeForce ( Vector3.forward * PlayerControl.main.str * 10 , ForceMode.Impulse );
	}

	void Update ( ) {
		transform.forward = rigidbody.velocity;
	}

	// Update is called once per frame
	void OnTriggerEnter ( ) {
		rigidbody.isKinematic = true;
		Destroy ( this );
	}

}
