﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {

	// Use this for initialization
	public Rigidbody rigidbody;

	void Start ( ) {
		rigidbody.AddRelativeForce ( Vector3.forward * PlayerControl.main.inte * 10 , ForceMode.Impulse );
	}

	void Update ( ) {

	}

	// Update is called once per frame
	void OnTriggerEnter ( ) {
		Destroy ( this.gameObject );
	}
}
