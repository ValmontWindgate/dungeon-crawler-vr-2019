﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class BaseChar : MonoBehaviour
{

    public int ActualHP;
    public int HP;
    public int DMG;
    public float speed;
    public float lastShot;
    public float reload;
    public float bulletSpeed;
    public float bulletSpawnTime;
    public bool left, right, up, down;
    public Animator anim;
    public GameObject character;
    public GameObject bullet;    
    public Transform bulletSpawn;



    public virtual void Shoot() { }    
    public virtual void Movement() { }
    public virtual int Damage() { return DMG; }
    public virtual void SetActualHP(int _acHP) { }
    public virtual void SetAnimations() { }

    public virtual void SetBoolsAnim(bool _left, bool _right, bool _up, bool _down)
    {
        left = _left;
        right = _right;
        up = _up;
        down = _down;
    }
    public virtual GameObject InstanciateObject(GameObject _gameObject, Vector3 _character, float _Xrot, float _Yrot, float _Zrot) { return null; }
    public virtual void Death(GameObject _GO)
    {
        _GO.SetActive(false);
    }    
}
