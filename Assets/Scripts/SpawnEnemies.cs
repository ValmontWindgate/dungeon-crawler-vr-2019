﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour {

	private GameObject[] spawnPoints;
	
	private bool roomCleaned = false;
	private bool spawnedEnemies = false;
	private EnemyList enemyList;
	private int rndNumber;

	// Use this for initialization
	void Start () 
	{
		enemyList = GetComponent<EnemyList>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (spawnedEnemies == false)
		{
			FillList();
		}		
	}

	public bool GetRoomCleaned() {return roomCleaned;}
	public void SetRoomCleaned(bool _roomCleaned) {roomCleaned = _roomCleaned;}
	public void CreateEnemies()
	{
		
		foreach(GameObject spawn in spawnPoints)
		{
			Instantiate(enemyList.enemies[rndNumber], spawn.transform, enemyList.enemies[rndNumber].transform);
		}
		spawnedEnemies = true;
	}
	public void FillList()
	{
		spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
		rndNumber = Random.Range(0, enemyList.enemies.Count);
		for (int i = 0; i < spawnPoints.Length ; i++)
		{
			Instantiate(enemyList.enemies[rndNumber], spawnPoints[i].transform);
		}
		spawnedEnemies = true;
	}
}
