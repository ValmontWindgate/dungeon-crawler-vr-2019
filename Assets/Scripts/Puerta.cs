﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Puerta : MonoBehaviour {

	public int escenaDestino;
	public int idPuerta;
	[HideInInspector]
	public Puerta entry;

	void OnTriggerEnter ( Collider infoAcceso ) {
		if ( infoAcceso.tag != "Player" ) return;
		transform.parent = null;
		DontDestroyOnLoad(this.gameObject);
		// idPuerta = -1;
		SceneControlLoadingBar.main.ChangeScene(escenaDestino);
		StartCoroutine ( WaitAndMoveCharacter ( ) );
	}

	IEnumerator WaitAndMoveCharacter ( ) {
		yield return null;

		MeshRenderer[] allRenders = GetComponentsInChildren<MeshRenderer>();

		foreach (MeshRenderer childRend in allRenders)
		{
			Destroy(childRend);
		}

		yield return new WaitForSeconds ( 1f );
		while ( SceneControlLoadingBar.main.isLoading ) {
			yield return null;
		}

		Puerta[] doors = GameObject.FindObjectsOfType<Puerta>();
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		Debug.Log ( "Encontradas " + doors.Length + " puertas en la nueva escena" );

		foreach (Puerta door in doors)
		{
			
			if (idPuerta == door.idPuerta && door.gameObject != this.gameObject )
			{
				entry = door;
				player.transform.position = entry.gameObject.transform.position + (entry.transform.forward * 2) + transform.up;
				player.transform.rotation = entry.transform.rotation;
				Debug.Log ( "Player movido a la puerta " + entry.gameObject.name );
			}	
		}
		if ( entry == null) Debug.LogError ( "No se encontró puerta con ID " + idPuerta + " en la escena " + escenaDestino);
		Destroy(this.gameObject);		
	}

}
