﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BasePlayer : MonoBehaviour {

    protected Animator anim;
    protected int maxHp;
    protected int currHp;
    protected int dmg;
    protected int armor;
    protected float lastAtk;
    protected float atkSpeed;
    protected float movSpeed;

    protected bool hasAttacked;

    public GameObject weapon;
    public Image hpImage;

    protected PlayerController cr;

    protected virtual void Awake()
    {
        PlayerControl.main.hp = PlayerControl.main.con + PlayerControl.main.str/2;
        maxHp = PlayerControl.main.hp + armor;
        currHp = maxHp;
        hpImage.fillAmount = currHp / maxHp;

        atkSpeed = 2.0f/PlayerControl.main.dex;
        movSpeed = PlayerControl.main.dex/(4.0f + armor);

        anim = GetComponentInChildren<Animator>();
        cr = GetComponent<PlayerController>();

        cr.velocidad += movSpeed;
    }

    protected virtual void Attack() { }
    protected virtual void Damage(int _value)
    {
        currHp -= _value;
        hpImage.fillAmount = currHp / maxHp;
        Death();
    }
    protected virtual void Death()
    {
        if (currHp < 1)
        {
            this.gameObject.SetActive(false);            
        }
    }

    public virtual int GetDMG() { return dmg; }
    public virtual int GetActualHP() { return currHp; }
    public virtual void SetActualHP(int _value) { currHp = _value; }
}
