﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArcherPlayer : BasePlayer {

    public int maxAmmo = 20;
    public int currAammo;

    public GameObject arrow;
    public GameObject arrowSpawner;
    public Text ammoTxt;

    protected override void Awake()
    {
        if ( PlayerControl.main.playerClass != 1 ) {
            this.enabled = false;
            weapon.SetActive ( false );
        }
        base.Awake();
        currAammo = maxAmmo;
        ammoTxt.text = currAammo.ToString();
        dmg = PlayerControl.main.dex/2;
    }
    
    // Update is called once per frame
    void Update()
    {
        Attack();
    }

    protected override void Attack()
    {
        if (Input.GetButtonDown("Fire1") && Time.time > lastAtk && currAammo > 0)
        {
            hasAttacked = true;
            anim.SetTrigger("Attack");
            GameObject tempArrow = Instantiate(arrow, arrowSpawner.transform.position, cr.camara.transform.rotation);
            currAammo--;
            ammoTxt.text = currAammo.ToString();
            lastAtk = Time.time + atkSpeed;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            hasAttacked = false;
        }
    }
}
