﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagePlayer : BasePlayer {

    public float maxMana;
    public float currMana;
    public int manaCost = 5;

    public GameObject magicMissile;
    public GameObject magicSpawner;

    public Image manaImage;

    protected override void Awake()
    {
        if ( PlayerControl.main.playerClass != 2 ) {
            this.enabled = false;
            weapon.SetActive ( false );
        } 
        base.Awake();
        maxMana = PlayerControl.main.wis + PlayerControl.main.inte/2;
        currMana = maxMana;
        dmg = PlayerControl.main.inte;
        manaImage.fillAmount = currMana / maxMana;

    }

	// Update is called once per frame
	void Update () {
		Attack();
	}

    protected override void Attack()
    {
        if (Input.GetButtonDown("Fire1") && Time.time > lastAtk && currMana > manaCost)
        {
            hasAttacked = true;
            anim.SetBool("Attack", hasAttacked);
            GameObject tempArrow = Instantiate(magicMissile, magicSpawner.transform.position, cr.camara.transform.rotation);
            currMana -= manaCost;
            manaImage.fillAmount = currMana / maxMana;
            lastAtk = Time.time + atkSpeed;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            hasAttacked = false;
        }

        if (currMana < maxMana)
        {
            currMana += ((float)PlayerControl.main.wis/3) * Time.deltaTime;
        }
    }
}
