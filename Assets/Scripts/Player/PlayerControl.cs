﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public static PlayerControl main;

    public int playerClass;

    [Header("Stats")]
    public int hp;
    public int mana;
    public int str;
    public int dex;
    public int con;
    public int inte;
    public int wis;
    public int cha;

    private void Awake()
    {
        if (main == null) main = this;
    }

    public void SetClass ( int selected ) {
        playerClass = selected;
    }

}
