﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorPlayer : BasePlayer {


    protected override void Awake()
    {
        Debug.Log ( PlayerControl.main.playerClass );
        if ( PlayerControl.main.playerClass != 0 ) {
            this.enabled = false;
            weapon.SetActive ( false );
        }
        base.Awake();
        dmg = PlayerControl.main.str;
    }

	// Update is called once per frame
	void Update () {
        Attack();
	}

    protected override void Attack()
    {
        if (Input.GetButtonDown("Fire1") && Time.time > lastAtk)
        {
            hasAttacked = true;
            anim.SetBool("attack", hasAttacked);
            lastAtk = Time.time + atkSpeed;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            hasAttacked = false;
        }
    }
}
