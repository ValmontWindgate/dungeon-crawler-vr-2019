﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour {
	
	public float tiempo = 0f;
	public float velocidad = 2f;
	public float tiempoCambioEscena = 30f;
	public int escenaDestino = 0;

	void Start ( ) {
		InvokeRepeating ( "RetornoMenu" , tiempoCambioEscena , tiempoCambioEscena );
	}

	void Update ( ) {
		tiempo += Time.deltaTime;
		transform.Translate ( Vector3.up * velocidad * Time.deltaTime );
		if ( Input.anyKeyDown && tiempo > 1 ) SceneManager.LoadScene ( 0 );
	}

	void RetornoMenu ( ) {
		SceneManager.LoadScene ( escenaDestino );
	}

}
