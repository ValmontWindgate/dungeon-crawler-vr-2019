﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Bat : Enemies {

	public Vector3 height;
	private Rigidbody batRB;

	// Use this for initialization
	void Start () 
	{
		DMG = 2;
		batRB = GetComponent<Rigidbody>();
		//playerScript = GameObject.Find("Manager").GetComponent<Player>();
		target = GameObject.FindGameObjectWithTag("Player");
		//heart_o_Meter = GameObject.Find("Manager").GetComponent<Heart_o_Meter>();		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (target != null){
		GoToPlayer(target.transform.position + height);

		if (Mathf.Abs(target.transform.position.magnitude - transform.position.magnitude) < 2)
		{
			batRB.AddForce(-1*(transform.position - target.transform.position), ForceMode.Impulse);
		}
		}

	}

	public override void GoToPlayer(Vector3 _plPos)
	{
		diference = _plPos - transform.position;
		
		transform.Translate ( diference.normalized*speed*Time.deltaTime , Space.World );

		transform.forward = diference;
	}

	public void Damaged(int _value)
	{
		ActualHP -= _value;
	}

	private void OnCollisionEnter(Collision other) 
	{
		if (other.collider.CompareTag("Player"))
		{
            if (playerScript.GetActualHP() > DMG)
            {
                playerScript.SetActualHP(playerScript.GetActualHP() - DMG);
            }
            else
            {
                playerScript.SetActualHP(0);
            }
        }		
	} 

	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Bullet"))
		{
			Damaged(playerScript.GetDMG());
		}

		if (ActualHP < 1)
		{
			Destroy(gameObject);
		}
	}
}
