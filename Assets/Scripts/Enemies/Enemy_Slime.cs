﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Slime : Enemies {

	// Use this for initialization
	void Start () 
	{
		//playerScript = GameObject.Find("Manager").GetComponent<Player>();
		target = GameObject.FindGameObjectWithTag("Player");
		//heart_o_Meter = GameObject.Find("Manager").GetComponent<Heart_o_Meter>();
		
		DMG = 1;
		ActualHP = 3;
		cam = Camera.main;
		target = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.LookAt(cam.transform);
		if (target != null)
		GoToPlayer(target.transform.position);			
	}

	public override void GoToPlayer(Vector3 _plPos)
	{
		diference = _plPos - transform.position;
		
		transform.Translate ( diference.normalized*speed*Time.deltaTime , Space.World );

		transform.forward = diference;
	}

	private void OnCollisionEnter(Collision other) 
	{
		if (other.collider.CompareTag("Player"))
		{
			playerScript.SetActualHP(playerScript.GetActualHP() - DMG);
		}		
	} 
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.GetComponent<Collider>().CompareTag("Bullet"))
		{
			ActualHP -= playerScript.GetDMG();
			if (ActualHP < 1)
			{
				Death(gameObject);
			}
		}		
	}	
}
