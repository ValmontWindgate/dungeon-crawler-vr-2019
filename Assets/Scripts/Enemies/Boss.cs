﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : BaseChar {

	public Animator animator;
	public GameObject player;
	protected Heart_o_Meter heart_o_Meter;
	public float changeTime = 2f;
	
	// Update is called once per frame
	void Update () {
		transform.LookAt ( player.transform );
	}
}
