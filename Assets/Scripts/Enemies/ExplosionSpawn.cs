﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSpawn : MonoBehaviour {

    public List<GameObject> prefabs;
    public int numObjects;
    public float force;
	
	void Start () {
		for ( int i=0; i<numObjects; i++ ) {
            int index = Random.Range(0, prefabs.Count);
            GameObject clone = Instantiate(
                prefabs[index],
                transform.position, Random.rotation);
            Vector3 dir = Random.insideUnitSphere * Random.Range(1,force);
            dir.y = Mathf.Abs(dir.y);
            clone.GetComponent<Rigidbody>().AddForce(dir, ForceMode.Impulse);
        }
	}

}
