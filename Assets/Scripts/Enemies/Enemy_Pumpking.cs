﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Pumpking : Enemies {

	// Use this for initialization
	void Start () 
	{

		//playerScript = GameObject.Find("Manager").GetComponent<Player>();
		target = GameObject.FindGameObjectWithTag("Player");
		//heart_o_Meter = GameObject.Find("Manager").GetComponent<Heart_o_Meter>();
		
		DMG = 1;
		speed = 2;
		ActualHP = 5;
		reload = 2;
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.LookAt(cam.transform);
		if(target != null){
		if ((target.transform.position - bulletSpawn.transform.position).magnitude < 5)
		{
		Shooting();
		} else {
			GoToPlayer(target.transform.position);
		}
		}
	}

	public override void GoToPlayer(Vector3 _plPos)
	{
		diference = _plPos - transform.position;
		
		transform.Translate ( diference.normalized*speed*Time.deltaTime , Space.World );

		transform.forward = diference;

	}

	private void Shooting()
	{
		if (Time.time > lastShot + reload)
		{
			InstantiateBullet();
			lastShot = Time.time;
		}
	}

	private void InstantiateBullet()
	{
		GameObject temp = Instantiate(bullet, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
		Rigidbody rb = temp.GetComponent<Rigidbody>();
		rb.AddForce(target.transform.position - bulletSpawn.transform.position + (transform.up), ForceMode.Impulse);

	}
	private void OnCollisionEnter(Collision other) 
	{
		if (other.collider.CompareTag("Player"))
		{
			playerScript.SetActualHP(playerScript.GetActualHP() - DMG);			
		}		
	}
	private void OnTriggerEnter(Collider other) 
	{
		if (other.GetComponent<Collider>().CompareTag("Bullet"))
		{
			ActualHP -= playerScript.GetDMG();
			if (ActualHP < 1)
			{
				Death(gameObject);
			}
		}		
	}	
}
