﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemyBullet : MonoBehaviour {

	private Player playerScript;
	private Heart_o_Meter heart_o_Meter;
	public int DMG = 1;

	public float lifeTime = 1.0f;
	// Use this for initialization
	void Start () 
	{

		playerScript = GameObject.Find("Manager").GetComponent<Player>();
		heart_o_Meter = GameObject.Find("Manager").GetComponent<Heart_o_Meter>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		DeactivateBullet();
	}

	private void DeactivateBullet()
	{
		Destroy(this.gameObject, lifeTime);
	}

	private void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag("Walls"))
		{
			Destroy(this.gameObject);
		}

		if (other.CompareTag("Player"))
		{	
			if (playerScript.GetActualHP() > DMG){
			playerScript.SetActualHP(playerScript.GetActualHP() - DMG);
			} else {
				playerScript.SetActualHP(0);
				}	
			heart_o_Meter.RemoveHearts(DMG);
			Destroy(this.gameObject);
		}
	}
}
