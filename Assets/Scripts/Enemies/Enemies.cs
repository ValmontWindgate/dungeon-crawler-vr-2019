﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Enemies : BaseChar {

    public GameObject target;
    public Vector3 diference;
    public Camera cam;
    protected BasePlayer playerScript;
    protected Heart_o_Meter heart_o_Meter;


    public virtual void GoToPlayer(Vector3 _plPos) { }
}