﻿
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneControlLoadingBar : MonoBehaviour {

	public LoadSceneMode loadMode = LoadSceneMode.Single;	// Nos permite elegir carga normal o aditiva
	public bool asyncMode = false;							// Nos permite elegir carga síncrona o asíncrona
	public Image loadingImage;

	public bool isLoading = false;

	public static SceneControlLoadingBar main;
	private void Awake() 
	{
		if (main == null)
		{
			main = this;
		}

		// loadingImage = GameObject.FindGameObjectWithTag("UILoading").GetComponent<Image>();
		loadingImage.fillAmount = 0;
	}

	public void ChangeScene ( int index ) {
		isLoading = true;
		// En modo asíncrono cargamos la escena en segundo plano, dejando que el
		//  juego pueda seguir ejecutando normalmente durante el proceso de carga
		if ( asyncMode ) {
			StartCoroutine ( ChangeSceneCourutine ( index ) );
		}
		// En modo síncrono cargamos la escena normal, tarde lo que tarde en cargar
		else {
			SceneManager.LoadScene ( index , loadMode );
		}
			
	}

	// Corrutina de cambio de escena, las corrutinas admiten BUCLES (while) y gracias a la sentencia
	//  yield return null NO bloquean la ejecución, eso nos permite cargar la nueva escena sin apenas
	//  bloquear el juego, pudiendo realizar alguna animación básica durante el tiempo de carga
	public IEnumerator ChangeSceneCourutine ( int index ) {
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync ( index , loadMode );
		while ( !asyncLoad.isDone ) {
			// Durante el proceso de carga rellenamos una barra de carga, para poder ver el progreso de la transición
			loadingImage.fillAmount = Mathf.Lerp ( loadingImage.fillAmount , asyncLoad.progress , 0.1f );
			yield return null;
		}	
		yield return new WaitForSeconds ( 1f );
		loadingImage.fillAmount = 1f;
		isLoading = false;
	}

}
