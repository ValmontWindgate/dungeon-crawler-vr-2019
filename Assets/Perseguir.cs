﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perseguir : MonoBehaviour {

	public GameObject objetivo;		// Aquí podemos arrastrar el objeto a perseguir
	public Vector3 diferencia;		// Aquí calculamos la diferencia de posición
	public float velocidad;			// Aquí ponemos la velocidad de avance
	
	// Update is called once per frame
	void Update () {
	
		diferencia = objetivo.transform.position - transform.position;

		transform.position = transform.position + diferencia*velocidad;

		transform.forward = diferencia;

	}
}
