﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorSetFloat : MonoBehaviour {

	public Animator animator;

	public string nombreFloat;
	public string nombreEje;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		animator.SetFloat (nombreFloat, Input.GetAxis ( nombreEje ) );
	}

}