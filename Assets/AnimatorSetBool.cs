﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorSetBool : MonoBehaviour {

	public Animator animator;

	public string nombreBool;
	public KeyCode teclaBool;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		animator.SetBool (nombreBool, Input.GetKey ( teclaBool) );
	}

}