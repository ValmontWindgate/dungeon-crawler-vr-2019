﻿using UnityEngine;

public class ControlPersonaje : MonoBehaviour {

	public float velocidad;			// Aquí le pondremos la velocidad de avance 
	public string inputAvance;		// Eje de teclado a leer
	public float lecturaInput;		// Aquí asignaremos el estado de la input
	public Vector3 ejeAvance;		// Dirección en la que se moverá
	
	// Update is called once per frame,,,,,.-,-
	void Update ( ) {
		
		lecturaInput = Input.GetAxis ( inputAvance );	// Leemos el estado de la input

		// Avanzamos en el eje de avance, lo que diga la lectura de la input, por la velocidad, en metros/segundo
		// transform.position = transform.position + ejeAvance*lecturaInput*velocidad*Time.deltaTime;
		transform.Translate ( ejeAvance*lecturaInput*velocidad*Time.deltaTime );

	}

}
