﻿using UnityEngine;

public class PruebaVariables : MonoBehaviour {

	// ZONA DE VARIABLES: public tipo nombre;
	public int dano;
	public float velocidad;
	public string dialogo;
	public bool esCritico;

	public Color colorSangreR;
	public Vector3 movimiento;

	// ZONA DE MÉTODOS
	void Update ( ) {
		Debug.Log ( "Mi daño es " + dano + " y mi velocidad es " + velocidad );
		
		velocidad = 0;
		dialogo = "CACA :D";
		esCritico = true;

		dano = dano + 1;

		transform.position = transform.position + movimiento;

	}

}
